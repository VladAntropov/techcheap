from pydantic.env_settings import BaseSettings


class Settings(BaseSettings):
    DATABASE_URI: str
    SECRET_KEY: str
    REDIS_HOST: str
    REDIS_PORT: int


settings = Settings(".env")
