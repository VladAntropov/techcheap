from enum import Enum


class KeyEnum(str, Enum):
    POSTS = "posts"
    USERS = "users"
