# TechCheap Blog Site

# Use:
Build containers:

```sh
$ docker-compose up -d --build
```

Migrations:

```sh
$ docker-compose exec backend aerich upgrade
```
